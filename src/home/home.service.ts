import {Injectable, NotFoundException} from '@nestjs/common';
import {PrismaService} from "../prisma/prisma.service";
import {CreateHomeDto, HomeResponseDto} from "./dto/home.dto";
import {take} from "rxjs";
import {PropertyType} from "@prisma/client";
import {IsArray, IsEnum, IsNotEmpty, IsNumber, IsPositive, IsString, ValidateNested} from "class-validator";
import {Type} from "class-transformer";

interface GetHomesParams {
    city?: string;
    price?: {
        gte?: number;
        lte?: number;
    }
    propertyType?: PropertyType
}

interface CreateHomeParams {
    address: string;
    numberOfBedrooms: number;
    numberOfBathrooms: number;
    city: string;
    price: number;
    landSize: number;
    propertyType: PropertyType;
    images: { url: string }[]
}

@Injectable()
export class HomeService {
    constructor(private readonly prismaService: PrismaService) {
    }

    async getHomes(filter: GetHomesParams): Promise<HomeResponseDto[]> {

        const homes = await this.prismaService.home.findMany({
            select: {
                id: true,
                address: true,
                city: true,
                price: true,
                property_type: true,
                number_of_bathrooms: true,
                number_of_bedrooms: true,
                images: {
                    select: {
                        url: true
                    },
                    take: 1
                }
            },
            where: filter,
        });

        if (!homes.length) {
            throw new NotFoundException()
        }
        return homes.map(
            (home) => {
                const fetchHome = {...home, image: home.images[0]?.url}
                delete fetchHome.images
                return new HomeResponseDto(fetchHome)
            }
        )
    }

    async getHomeById(id: number): Promise<HomeResponseDto> {
        const home = await this.prismaService.home.findMany({
            select: {
                id: true,
                address: true,
                city: true,
                price: true,
                property_type: true,
                number_of_bathrooms: true,
                number_of_bedrooms: true,
                images: {
                    select: {
                        url: true
                    },
                    take: 1
                }
            },
            where: {
                id: id
            }
        });
        if (!home) {
            throw new NotFoundException()
        }
        const fetchHome = {...home[0], image: home[0].images[0]?.url}
        delete fetchHome.images
        return new HomeResponseDto(fetchHome)
    }

    async createHome({address, numberOfBathrooms, numberOfBedrooms, city, price, landSize, propertyType, images}: CreateHomeParams) {
        const home = await this.prismaService.home.create({
            data: {
                address,
                number_of_bathrooms: numberOfBathrooms,
                number_of_bedrooms: numberOfBedrooms,
                city,
                price,
                land_size: landSize,
                property_type: propertyType,
                realtor_id: 4
            }
        })

        const homeImages = images.map((image) => {
            return {...image, home_id: home.id};
        })

        await this.prismaService.image.createMany({data: homeImages})

        return new HomeResponseDto(home)
    }
}
