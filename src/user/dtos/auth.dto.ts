import {IsString, IsNotEmpty, IsEmail, MinLength, Matches, IsEnum, IsOptional} from "class-validator"
import {UserType} from "@prisma/client";


export class SignupDto{
    @IsString()
    @IsNotEmpty()
    name:string;

    @Matches(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/,{
        message:'wrong phone'
    })
    phone:string;

    @IsEmail()
    email:string;

    @IsString()
    @MinLength(5)
    password:string;

    @IsOptional()
    @IsNotEmpty()
    @IsString()
    productKey?: string
}
export class SigninDto{
    @IsEmail()
    email:string;

    @IsString()

    password:string;
}

export  class GenerateProductKey{
    @IsEmail()
    email:string;

    @IsEnum(UserType)
    userType: UserType;
}